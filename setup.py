"""
Build with:
     python setup.py build_ext --inplace
"""

## From Matt's original code this line must be moved above in order to not throw the "ext_modules... tuple..." error
from setuptools.dist import Distribution

from Cython.Build import cythonize

#from distutils.core import setup
from setuptools import setup

from distutils.extension import Extension
import os
import numpy

class BinaryDistribution(Distribution):
    def is_pure(self):
        return False


os.environ["CC"]='cc'

extensions = Extension("_transform",["mycurrents/_transform.pyx"],
    include_dirs=[numpy.get_include()],
    library_dirs=[],
    #extra_compile_args=['-O3'],
)

setup(
    name = "mycurrents",
    version='latest',
    description='Ocean mooring data IO utilities',
    author='Matt Rayson',
    author_email='matt.rayson@uwa.edu.au',
    ext_modules = cythonize(extensions),
    packages = ['mycurrents'],
    install_requires=['numpy','scipy','matplotlib','netcdf4','xarray','dolfyn'],
    include_package_data=True,
    distclass=BinaryDistribution,
)
